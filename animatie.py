
""" works for single line """
#
# import sys
# import time
#
#
# a = 0
# for x in range(0, 10):
#     a = a + 1
#     b = ("Loading" + "." * a + "\n--------------------------")
#     # b = ("Loading" + "." * a)
#     # \r prints a carriage return first, so `b` is printed on top of the previous line.
#     sys.stdout.write('\r'+b)
#     # sys.stdout.flush()
#     time.sleep(0.3)

""" works in terminal"""

# import random
# import time
# import os
#
#
# def clear_screen():
#     os.system('cls' if os.name == 'nt' else 'clear')
#
# a = 0
# while True:
#     clear_screen()
#
#     statement = """
#     Line {}
#     Line {}
#     Line {}
#     Value = {}
#     """.format(random.random(), random.random(), random.random(), a)
#     print(statement, end='\r')
#     time.sleep(1)
#     a += 1

""" Duwt oude content uit beeld """

# def clearscreen(numlines=100):
#     print('\n' * numlines)

