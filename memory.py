import random
from termcolor import colored

letter_kaartjes = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    # "G",
    # "H",
    # "I",
    # "J",
    # "K",
    # "L",
]

plaatjes_kaartjes = [
    "👑",
    "😊",
    "❤️",
    "🌹",
    # "🎉",
    # "🐶",
    # "🌺",
    # "🎨",
    # "☀",
    # "♡",
    # "✂",
    # "✈",
]

getallen = "1234567890"


def leg_kaartjes_neer(kaartjes):
    # Elk kaartje 2 keer
    kaartjes = kaartjes + kaartjes

    # Schud de kaartjes
    random.shuffle(kaartjes)

    # Elk kaartje heeft voorkant en achterkant
    gelegde_kaartjes = []
    for tel, kaartje in enumerate(kaartjes):
        # voorkant, is_gevonden, achterkant
        gelegde_kaartjes.append((str(tel + 1), False, kaartje))

    return gelegde_kaartjes


def kleur(voorkant):
    if voorkant[0] == "*":
        return colored(voorkant, "magenta")
    if voorkant[0] in getallen:
        return colored(voorkant, "yellow")
    else:
        return colored(voorkant, "blue")


def toon(kaartjes, breed):
    voorkanten = []
    for k in kaartjes:
        if k[1]:
            # gevonden
            voorkanten.append("*")
        else:
            voorkanten.append(k[0])

    print("")
    for i, vk in enumerate(voorkanten):
        # getal met dubbele cijfers, maar 1 spatie er bij
        if len(vk) > 1:
            print(kleur(vk), end="  ")
        else:
            print(kleur(vk), end="   ")

        if (i+1) % breed == 0:
            print("")
            print("")
    print("")


def alle_om(kaartjes):
    om_kaartjes = []
    for k in kaartjes:
        if k[0][0] in getallen:
            om_kaartjes.append(k)
        else:
            # Draai kaartje om en voeg toe
            om_kaartjes.append(k[::-1])

    return om_kaartjes


def kaartje_toont_getal(k):
    return k[0][0] in getallen


def match(kaartjes, gevonden_letters):

    is_gevonden = False
    omgedraaide_kaarten = []
    aangepaste_kaarten = []
    for index, k in enumerate(kaartjes):
        if not kaartje_toont_getal(k):
            # omgedraaide kaart
            omgedraaide_kaarten.append(k)

            # Als we al omgedraaide kaart hadden, kunnen we vergelijken
            if len(omgedraaide_kaarten) > 1:
                if omgedraaide_kaarten[0][0][0] == omgedraaide_kaarten[1][0][0]:
                    is_gevonden = True
                    gevonden_letters.append(omgedraaide_kaarten[0][0][0])

                    # Zet op gevonden
                    omgedraaide_kaarten[0] = ("*", True, "*")
                    omgedraaide_kaarten[1] = ("*", True, "*")

    if is_gevonden:
        # Zet op gevonden
        for k in kaartjes:
            if not kaartje_toont_getal(k):
                k = k[0], True, k[2]
                aangepaste_kaarten.append(k)
            else:
                aangepaste_kaarten.append(k)
    else:
        aangepaste_kaarten = kaartjes

    return gevonden_letters, aangepaste_kaarten


def alles_geraden(kaartjes):
    for k in kaartjes:
        if not k[1]:
            return False
    return True


def speel(geleverde_kaartjes, breed=4):
    gelegde_kaartjes = leg_kaartjes_neer(geleverde_kaartjes)

    toon(gelegde_kaartjes, breed)

    keer_gedraaid = 0
    gevonden_letters = []
    beurten = 0
    while True:
        if keer_gedraaid < 2:
            geldige_keuze = False
            while not geldige_keuze:
                keuze = input("welke?")
                if keuze != "" and keuze[0] in "1234567890":
                    # TODO: beter..
                    geldige_keuze = True
                else:
                    toon(gelegde_kaartjes, breed)

        else:
            # kijk of iets gevonden
            gevonden_letters, gelegde_kaartjes = match(gelegde_kaartjes, gevonden_letters)

            # 2 keer omgedraaid, en weer alles om
            gelegde_kaartjes = alle_om(gelegde_kaartjes)
            keer_gedraaid = 0
            keuze = ""
            _ = input("terug draaien..")
            beurten += 1
            toon(gelegde_kaartjes, breed)
        print("gevonden: " + ",".join(gevonden_letters))

        if keuze:
            # draai kaartje om
            kaartjes = []
            for k in gelegde_kaartjes:
                if str(k[0]) == keuze:
                    print("kaartje gevonden: " + k[0])
                    # Draai kaartje om
                    kaartjes.append(k[::-1])
                else:
                    kaartjes.append(k)
            gelegde_kaartjes = kaartjes
            toon(gelegde_kaartjes, breed)
            keer_gedraaid += 1

        if alles_geraden(gelegde_kaartjes):
            print("Gehaald!\nIn " + str(beurten) + " beurten")
            break


# speel(letter_kaartjes, breed=4)
speel(plaatjes_kaartjes, breed=3)
