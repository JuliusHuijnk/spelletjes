import random
import string
from termcolor import colored

# TODO: lijst gebruikte woorden op alfabet en in een grid voor leesbaarheid
korte_woorden = [
    "bal",
    "bel",
    "jas",
    "kip",
    "dop",
    "bes",
    "op",
    "in",
    "zin",
    "som",
    "juf",
    "tak",
    "rok",
    "bak",
    "doe",
    "oud",
    "uit",
    "oor",
    "bos",
    "zee",
    "uur",
    "aap",
]
basis_woorden = [
    "fiets",
    "bal",
    "drop"
]

school_woorden = [
    "pen",
    "juf",
    "meester",
    "schoolbord",
    "mila",
    "puk",
    "indy",
    "karin",
    "fee",
    "potlood",
    "schommel",
    "school",
    "huiswerk",
    "spelen",
    "les",
    "samen",
    "pauze",
    "vakje",
]

elsa_woorden = [
   "het",
    "pappa",
    "gezin",
    "mama",
    "annelie",
    "zusje",
    "lieverd",
    "kusjes",
    "kussen",
    "knikkers",
    "zakje",
    "hondje",
    "kussenhuis",
    "slapen",
    "vierkantje",
    "driehoekje",
    "plat",
    "dik",
    "mandarijn",
    "tafeltje",
    "stoeltje",
    "plankje",
    "kast",
    "glitter",
    "huis",
    "plat",
    "papier",
    "vogelhuisje",
    "thee"
]

allerlei_woorden = [
    "ballon",
    "droom",
    "blokken",
    "elsa",
    "julius",
    "mariska",
    "boterham",
    "bloem",
    "strand",
    "potlood",
    "kaarten",
    "zwembad",
    "pindakaas",
    "glijbaan",
    "piano",
    "vork",
    "lepel",
]

woorden_kleuren = [
    "blauw",
    "geel",
    "oranje",
    "groen",
    "zilver",
    "goud",
    "wit",
    "paars",
    "zwart",
    "roze",
    "rood",
]

woorden_aan_tafel = [
    "bord",
    "lepel",
    "mes",
    "vork",
    "bestek",
    "beker",
    "glas",
    "rietje",
]

woorden_badkamer = [
    "tandenborstel",
    "tandenpasta",
    "poetsen",
    "bad",
    "wc",
    "tegels",
    "spiegel",
    "kam",
    "borstel",
    "kraan"
]


alle_woorden = woorden_kleuren + allerlei_woorden + woorden_aan_tafel + woorden_kleuren + school_woorden


def grid_omdraaien(grid):
    omgedraaid_grid = []
    for i in range(len(grid[0])):
        r = ""
        for j in range(len(grid)):
            r += grid[j][i]
        omgedraaid_grid.append(r)

    return omgedraaid_grid


def woord_past_vertikaal(start_regel_nummer, start_plek, grid, woord):
    overlapt = False
    for regel_nummer, regel in enumerate(grid):
        for grid_plek, letter in enumerate(regel):
            if grid_plek == start_plek and start_regel_nummer == regel_nummer:
                # Is er genoeg ruimte over
                aantal_regels_beschikbaar = len(grid) - regel_nummer
                if len(woord) > aantal_regels_beschikbaar:
                    return False, overlapt

                # Zijn de letters nog vrij, of gelijk aan letter van woord op die plek
                for letter_nr, woord_letter in enumerate(woord):
                    letter_in_grid = grid[start_regel_nummer + letter_nr][start_plek]
                    if letter_in_grid == woord_letter:
                        overlapt = True

                    if letter_in_grid != "*" and letter_in_grid != woord_letter:
                        return False, overlapt
    return True, overlapt


def woord_past_horizontaal(plek, regel, woord):
    over = len(regel) - len(woord) - plek
    overlapt = False

    if over >= 0:  # past
        # is plek vrij
        overlap = regel[plek: len(woord) + plek]
        for overlap_plek, letter in enumerate(overlap):
            if letter == woord[overlap_plek]:
                overlapt = True

            if letter != "*" and letter != woord[overlap_plek]:
                return False, overlapt
        return True, overlapt
    return False, overlapt


def mogelijke_startplekken_horizontaal(grid, woord):
    # Aanname is dat alle letters een startplek kan zijn
    mogelijke_plekken = []
    overlappende_mogelijke_plekken = []
    for regel_nummer, regel in enumerate(grid):
        for plek, letter in enumerate(regel):
            past, overlapt = woord_past_horizontaal(plek, regel, woord)
            if past:
                mogelijke_plekken.append((regel_nummer, plek))
                if overlapt:
                    overlappende_mogelijke_plekken.append((regel_nummer, plek))

    # Voorkeur overlappende
    if overlappende_mogelijke_plekken:
        return overlappende_mogelijke_plekken

    return mogelijke_plekken


def mogelijke_startplekken_vertikaal(grid, woord):
    mogelijke_plekken = []
    overlappende_mogelijke_plekken = []
    for regel_nummer, regel in enumerate(grid):
       for plek, letter in enumerate(regel):
           past, overlapt = woord_past_vertikaal(regel_nummer, plek, grid, woord)
           if past:
                mogelijke_plekken.append((regel_nummer, plek))
                if overlapt:
                   overlappende_mogelijke_plekken.append((regel_nummer, plek))

    # Voorkeur overlappende
    if overlappende_mogelijke_plekken:
        return overlappende_mogelijke_plekken

    return mogelijke_plekken


def vind_start_plek_en_richting(grid, woord):

    startplekken_horizontaal = mogelijke_startplekken_horizontaal(grid, woord)
    startplekken_vertikaal = mogelijke_startplekken_vertikaal(grid, woord)

    # kies horizontaal of vertikaal
    if startplekken_horizontaal and startplekken_vertikaal:
        gevonden = True
        richting = random.choice(["horizontaal", "vertikaal"])
        if richting == "horizontaal":
            startplek = random.choice(startplekken_horizontaal)
            return gevonden, startplek, "horizontaal"
        else:
            startplek = random.choice(startplekken_vertikaal)
            return gevonden, startplek, "vertikaal"

    elif not startplekken_horizontaal and not startplekken_horizontaal:
        gevonden = False
        return gevonden, 0, "horizontaal"

    elif startplekken_horizontaal:
        gevonden = True
        startplek = random.choice(startplekken_horizontaal)
        return gevonden, startplek, "horizontaal"

    else:  # Vertikaal
        gevonden = True
        startplek = random.choice(startplekken_vertikaal)
        return gevonden, startplek, "vertikaal"


def plaats_woord_horizontaal_op_grid(grid, start_plek, woord):
    woord_regel, woord_plek = start_plek
    nieuw_grid = []
    for regel_nummer, regel in enumerate(grid):
        if regel_nummer == woord_regel:
            for plek, letter in enumerate(regel):
                if plek == woord_plek:
                    start = regel[:woord_plek]
                    midden = woord
                    eind = regel[woord_plek + len(woord):]
                    nieuw_grid.append(start + midden + eind)
        else:
            nieuw_grid.append(regel)
    return nieuw_grid


def plaats_in_grid(grid, woord):
    # Aanname, grid is 1 regel
    gebruikt_woord = None
    gevonden, start_plek, richting = vind_start_plek_en_richting(grid, woord)
    if not gevonden:
        return gebruikt_woord, grid
    if richting == "horizontaal":
        # plaats woord, startend bij start_plek
        grid = plaats_woord_horizontaal_op_grid(grid, start_plek, woord)
        gebruikt_woord = woord
        return gebruikt_woord, grid
    else:  # vertikaal
        omgedraaid_grid = grid_omdraaien(grid)
        omgedraaide_start_plek = start_plek[::-1]
        aangepast_grid = plaats_woord_horizontaal_op_grid(omgedraaid_grid, omgedraaide_start_plek, woord)
        gebruikt_woord = woord
        terugedraaid_grid = grid_omdraaien(aangepast_grid)
        return gebruikt_woord, terugedraaid_grid


def willekeurige_letter():
    return random.choice(string.ascii_lowercase)


def vul(grid, vul_lijst, willekeurig=True):
    # aanname is dat grid 1 regel is
    nieuw_grid = []
    for regel in grid:
        nieuwe_regel = ""
        for letter in regel:
            if letter == "*":
                if willekeurig or len(vul_lijst) == 0:
                    nieuwe_regel += willekeurige_letter()
                else:
                    vul_letter = vul_lijst[:1]
                    vul_lijst = vul_lijst[1:]
                    nieuwe_regel += vul_letter
            else:
                nieuwe_regel += letter
        nieuw_grid.append(nieuwe_regel)
    return nieuw_grid


def maak_grid(woorden, breed=10, hoog=10, vul_lijst=""):
    grid = [breed * "*" for _ in range(hoog)]

    gebruikte_wooorden = []
    for woord in woorden:
        if woord not in gebruikte_wooorden:
            gebruikt_woord, grid = plaats_in_grid(grid, woord)
            if gebruikt_woord:
                gebruikte_wooorden.append(gebruikt_woord)
    if vul_lijst:
        willekeurig=False
    else:
        willekeurig=True

    grid = vul(grid, vul_lijst, willekeurig)
    return grid, gebruikte_wooorden


def speel(woorden, breed, hoog, vul_lijst=""):
    tekst = ""
    random.shuffle(woorden)
    grid, gebruikte_woorden = maak_grid(woorden, breed, hoog, vul_lijst)
    print("")
    print("")
    for regel in grid:
        regel_upper = " ".join(regel.upper())
        print(regel_upper)
        tekst += regel_upper + "\n"

    streep = "-----------------"
    print(streep)
    tekst += streep + "\n"

    gebruikte_woorden.sort()
    gebruikte_woorden_titel = "gebruikte woorden:"
    print(gebruikte_woorden_titel)
    tekst += gebruikte_woorden_titel + "\n"

    lijst_gebruikte_woorden = ", ".join(gebruikte_woorden).upper()
    print(lijst_gebruikte_woorden)
    tekst += lijst_gebruikte_woorden + "\n"

    return tekst

    # while True:
    #     toon_woord = input("toon woord: ")
    #     nieuw_grid = []
        # for regel in grid:
        #     if toon_woord.lower() in regel:
        #         # nieuwe_regel = regel.replace(toon_woord.lower(), len(toon_woord) * "*")
        #         gebruikte_woorden.remove(toon_woord.lower())
        #     else:
        #         nieuwe_regel = regel
        #     nieuw_grid.append(nieuwe_regel)
        # if woord_vertikaal_in_grid(toon_woord, grid):
        #     # nieuw_grid = highlight_woord_vertikaal_in_grid()
        #     gebruikte_woorden.remove(toon_woord.lower())

        # grid = nieuw_grid
        #
        # print("")
        # print("")
        # for regel in grid:
        #     print(" ".join(regel.upper()))
        # print("-----------------")
        # print("gebruikte woorden:")
        # for word in gebruikte_woorden:
        #     print("- " + word.upper())


# speel(woorden_kleuren, breed=5, hoog=5)
# tekst = speel(alle_woorden, breed=10, hoog=10)
tekst = speel(elsa_woorden, breed=14, hoog=14)
# tekst2 = speel(korte_woorden, breed=8, hoog=8)
# tekst3 = speel(korte_woorden, breed=8, hoog=8)
# tekst4 = speel(korte_woorden, breed=8, hoog=8)

with open("korte_woorden.txt", 'w') as f:
    f.write(tekst)
    f.write("\n")
    f.write("\n")
    # f.write(tekst2)
    # f.write("\n")
    # f.write("\n")
    # f.write(tekst3)
    # f.write("\n")
    # f.write("\n")
    # f.write(tekst4)
