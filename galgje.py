
# Galgje
import random
from termcolor import colored

zielig = "----------------- \n" \
         "-               - \n" \
         "-  X       X    - \n" \
         "-  `   v   `    - \n" \
         "-  _-------_    - \n" \
         "----------------- \n"

blij = "----------------- \n" \
       "-               - \n" \
       "-  X       X    - \n" \
       "-      v        - \n" \
       "-  [_______]    - \n" \
       "----------------- \n"

woorden = [
    "ballon",
    "droom",
    "blokken",
    "elsa",
    "julius",
    "mariska",
    "boterham",
    "bloem",
    "strand",
    "potlood",
    "kaarten",
    "zwembad",
    "pindakaas",
    "glijbaan",
    "piano",
    "vork",
    "lepel",
]

woorden_kleuren = [
    "blauw",
    "geel",
    "oranje",
    "groen",
    "zilver",
    "goud",
    "wit",
    "paars",
    "zwart",
    "roze",
    "rood",
]

woorden_aan_tafel = [
    "bord",
    "lepel",
    "mes",
    "vork",
    "bestek",
    "beker",
    "glas",
    "rietje",
]

woorden_badkamer = [
    "tandenborstel",
    "tandenpasta",
    "poetsen",
    "bad",
    "wc",
    "tegels",
    "spiegel",
    "kam",
    "borstel",
    "kraan"
]

alle_woorden = woorden + woorden_kleuren


AANTAL_BEURTEN = 10


def print_wat_je_weet(gok):
    print("\nWat je weet: " + " ".join(map(lambda l: colored(l, "yellow") if l == "*" else colored(l, "blue"), gok)))


def stel_vraag(woord):
    gok = len(woord) * "*"
    beurten = AANTAL_BEURTEN
    geprobeerde_letters = ""

    print_wat_je_weet(gok)

    while "*" in gok:
        antwoord_l = input("\nWelke letter? ")

        if len(antwoord_l) == 1:
            if antwoord_l in geprobeerde_letters:
                print("\nAl geprobeerd..")
                continue
            else:
                geprobeerde_letters += antwoord_l
        else:
            print("\nVul 1 letter in..")
            continue

        nieuwe_gok = ''.join(list(map(lambda gok_l, woord_l : woord_l if woord_l == antwoord_l else gok_l, gok, woord)))
        gevonden = nieuwe_gok != gok
        gok = nieuwe_gok

        print_wat_je_weet(gok)

        print("\nGeprobeerde letter: " + geprobeerde_letters)
        if gevonden:
            print(blij)
            print(f'\nnog {beurten} beurten')
        else:
            beurten -= 1
            if beurten == 0:  # Niet geraden
                print(colored("\nBEURTEN OP. Het woord was " + colored(woord, "white") + ". Nog een keer :) \n", "red"))
                return False
            else:
                print(f'\nnog {beurten} beurten')

    else:  # Geraden
        print(colored("\nGERADEN!!! Nog een keer :) \n", "green"))
        return True


def blijf_vragen(woorden_lijst):
    lijst = woorden_lijst
    score = 0
    while len(lijst) > 0:
        woord = random.choice(lijst)
        lijst.remove(woord)
        gelukt = stel_vraag(woord)

        if gelukt:
            score += 1
    print(f"\n Het is spel is afgelopen. Score: {score}")


blijf_vragen(woorden_aan_tafel)