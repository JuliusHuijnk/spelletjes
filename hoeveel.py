import random
import datetime
from random import randint

""" Hoeveel zie je? """
from random import randint

# Plaatjes -------------------------------------------------------------------------------------------------------------

zielig = "----------------- \n" \
         "-               - \n" \
         "-  -       -    - \n" \
         "-  `   v   `    - \n" \
         "-  _-------_    - \n" \
         "----------------- \n"

blij = "----------------- \n" \
       "-               - \n" \
       "-  o   o   o    - \n" \
       "-      >        - \n" \
       "-  [_______]    - \n" \
       "----------------- \n"


slaap = "----------------- \n" \
        "-               - \n" \
        "-  -       -    - \n" \
        "-      v        - \n" \
        "-  _________    - \n" \
        "----------------- \n"

# ----------------------------------------------------------------------------------------------------------------------

tel_letter = "#"
anders = "0"


def stel_vraag(aantal):
    samen = tel_letter + " " + anders + " "
    print(aantal * samen)
    print("")
    antwoord = input("Hoeveel " + tel_letter + " ?")

    if antwoord == str(aantal):
        print(blij)
    else:
        print(zielig)
    print("-----------------------------")


def random_vraag(max):
    aantal = random.choice(range(1,max))
    stel_vraag(aantal)


def blijf_spelen(aantal_keer, max_getal):
    dit_uur = datetime.datetime.now().hour
    avond = 1
    if dit_uur > avond:
        print(slaap)

    print("\nwe gaan " + str(aantal_keer) + " keer spelen.")
    print("grootste getal is: " + str(max_getal) + "\n")
    for _ in range(aantal_keer):
        random_vraag(max_getal)

# ----------------------------------------------------------------------------------------------------------------------


blijf_spelen(3, 10)
