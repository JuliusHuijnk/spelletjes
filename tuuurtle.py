import turtle
from random import choice
t = turtle.Turtle()
t.shape("turtle")
t.screen.bgcolor("black")
t.speed(1900)

for i in range(180):
    if i % 20 == 0:
        t.goto(choice([-400, -300, -200, -100, 0, 100, 200, 300, 400]),
        choice([-300, -200, -100, 0, 100, 200, 300]))

    t.forward(100)
    t.right(90)
    t.forward(50)
    t.color(choice(["orange", "pink", "red", "yellow"]))
    t.write(choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+"), font=("Calibri", choice([4, 20, 30, 40, 100]), "bold"))
    t.right(90)
    t.forward(50)
    t.right(i - choice([8, 5]))
    t.color(choice(["orange", "pink", "red", "yellow"]))
    t.write(choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+"), font=("Calibri", choice([4, 20, 30, 40, 100]), "bold"))
    t.backward(choice([5, 8]))
    t.right(3)
    t.pensize(choice([1, 2]))
    t.color(choice(["orange", "pink", "red", "yellow"]))
    t.write(choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+"), font=("Calibri", choice([4, 20, 30, 40, 100]), "bold"))
