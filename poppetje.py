def poppetje(haar="M", oog="o", neus="v", mond="x"):
    print(
        "  MMMMMMM \n"
        "MMMMMMMMMMM \n"
        "|         | \n"
        "| o     o | \n"
        "|     v   | \n"
        "| x     x | \n"
        "|  xxxxx  | \n"
        "+---------+ \n"
            .replace("M", haar)
            .replace("o", oog)
            .replace("v", neus)
            .replace("x", mond)
    )


# poppetje(haar="|", oog="@", neus="}", mond="*")
# poppetje(haar="F", oog="O", neus="v", mond="X")
# poppetje(haar="-", oog="Q", neus="'", mond=" ")
# poppetje(haar="|", oog="c", neus="1", mond="\\")
poppetje(haar="R", oog="T", neus="Y", mond="=")
