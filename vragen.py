from random import shuffle

familie_vragen =[
    ("Hoe heet pappa?", "Julius"),
    ("Hoe heet mamma?", "Mariska")
]


def vragen_spel(max_vragen=10):
    aantal_gesteld = 0
    punten = 0
    shuffle(familie_vragen)
    vragen = familie_vragen

    while vragen and len(vragen) > 0 and aantal_gesteld < max_vragen:

        vraag, juiste_antwoord = vragen.pop()
        antwoord = input(vraag + "\n")

        if antwoord.lower() == juiste_antwoord.lower():
            punten += 1
            print("Goedzo")
        else:
            print("Helaas")
        aantal_gesteld += 1

    print(f"Einde! Je hebt {punten} punten")


vragen_spel()
