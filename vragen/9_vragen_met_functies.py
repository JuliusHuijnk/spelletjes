punten = 0
er_bij = 10
altijd_goed = "x"


def goed_of_fout(antwoord, juiste_antwoord, punten):
    if antwoord == juiste_antwoord or antwoord == altijd_goed:
        punten += er_bij
        print(f"Goed zo. Nu heb je {punten} punten")
    else:
        print("Niet goed helaas")
    return punten


def stel_vraag(vraag):
    antwoord = ""
    while antwoord == "":
        antwoord = input(vraag)
    return antwoord

# ----------------------------------------------------------------------------------------------------------------------

vraag = "hoe heet pappa? "
juiste_antwoord = "julius"
antwoord = stel_vraag(vraag)

punten = goed_of_fout(antwoord, juiste_antwoord, punten)

# ----------------------------------------------------------------------------------------------------------------------

vraag = "hoe heet mama? "
juiste_antwoord = "mariska"
antwoord = stel_vraag(vraag)

punten = goed_of_fout(antwoord, juiste_antwoord, punten)

# ----------------------------------------------------------------------------------------------------------------------

vraag = "hoe heet zus van annelie? "
juiste_antwoord = "elsa"
antwoord = stel_vraag(vraag)


punten = goed_of_fout(antwoord, juiste_antwoord, punten)

# ----------------------------------------------------------------------------------------------------------------------

print("Klaar!")
print(f"Je hebt {punten} punten")

