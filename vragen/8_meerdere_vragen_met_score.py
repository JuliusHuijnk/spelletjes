punten = 0

# ----------------------------------------------------------------------------------------------------------------------

vraag = "hoe heet pappa?"
juiste_antwoord = "julius"

antwoord = input(vraag)

if antwoord == juiste_antwoord:
    punten_er_bij = 10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
    punten += punten_er_bij
    print(f"Goed zo. {punten_er_bij} punten er bij, nu heb je {punten} punten")
else:
    print("Niet goed helaas")

# ----------------------------------------------------------------------------------------------------------------------

vraag = "hoe heet mama? "
juiste_antwoord = "mariska"

antwoord = input(vraag)

if antwoord == juiste_antwoord:
    punten += 1
    print(f"Goed zo. 1 punt er bij, nu heb je {punten} punten")
else:
    print("Niet goed helaas")

# ----------------------------------------------------------------------------------------------------------------------

vraag = "hoe heet zus van annelie? "
antwoord = input(vraag)

juiste_antwoord = "elsa"

if antwoord == juiste_antwoord:
    punten += 1
    print(f"Goed zo. 1 punt er bij, nu heb je {punten} punten")
else:
    print("Niet goed helaas")

# ----------------------------------------------------------------------------------------------------------------------

print("Klaar!")
print(f"Je hebt {punten} punten")

