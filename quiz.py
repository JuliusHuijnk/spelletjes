import random
from termcolor import colored

# Plaatjes -------------------------------------------------------------------------------------------------------------

zielig = "----------------- \n" \
         "-               - \n" \
         "-  X       X    - \n" \
         "-  `   v   `    - \n" \
         "-  _-------_    - \n" \
         "----------------- \n"

blij = "----------------- \n" \
       "-               - \n" \
       "-  X       X    - \n" \
       "-      v        - \n" \
       "-  [_______]    - \n" \
       "----------------- \n"

slaap = "----------------- \n" \
       "-               - \n" \
       "-  -       -    - \n" \
       "-      v        - \n" \
       "-  _________    - \n" \
       "----------------- \n"

# Vragen ---------------------------------------------------------------------------------------------------------------


vragen_en_antwoorden = [
    ("1 + 1", "2"),
    ("2 + 2", "4"),
    ("2 - 1", "1"),
    ("6 + 2", "8"),
    ("4 + 4", "8"),
    ("5 + 4", "9"),
    ("2 - 2", "0"),
]


# ----------------------------------------------------------------------------------------------------------------------


def teken(iets):
    print(colored(iets, 'red'))


def stel_vraag(vraag):
    vraag_tekst, juiste_antwoord = vraag
    antwoord = input(vraag_tekst + "\nJouw antwoord? ")

    if antwoord == "moe":
        teken(slaap)
    else:
        if juiste_antwoord == antwoord:
            teken(blij)
            return True
        else:
            teken(zielig)
            return False


def vraag_alle(vragen, willekeurig=False):
    if willekeurig:
        random.shuffle(vragen)

    hoeveel_goed = 0
    for vraag in vragen:
        is_goed = stel_vraag(vraag)
        if is_goed:
            hoeveel_goed = hoeveel_goed + 1

        print("*********************************")
        teken("* Totaal goed: " + str(hoeveel_goed) + " van de " + str(len(vragen)))
        print("*********************************\n")


def vraag_eentje(vragen):
    vraag = random.choice(vragen)
    stel_vraag(vraag)

# ----------------------------------------------------------------------------------------------------------------------

# vraag_eentje(vragen_en_antwoorden)

vraag_alle(vragen_en_antwoorden, willekeurig=True)

